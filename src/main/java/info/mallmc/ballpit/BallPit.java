package info.mallmc.ballpit;

import info.mallmc.ballpit.api.BallPitGame;
import info.mallmc.ballpit.api.IBallPitGame;
import info.mallmc.ballpit.api.Map;
import info.mallmc.ballpit.commands.GameCommand;
import info.mallmc.ballpit.commands.GameModeCommand;
import info.mallmc.ballpit.commands.MapCommand;
import info.mallmc.ballpit.commands.Spectator;
import info.mallmc.ballpit.events.CountdownFinishEvent;
import info.mallmc.ballpit.events.PlayerJoinEvents;
import info.mallmc.ballpit.events.core.EventGameStateChange;
import info.mallmc.ballpit.util.CountDownManager;
import info.mallmc.ballpit.util.CountdownHelper;
import info.mallmc.ballpit.util.GameModeManager;
import info.mallmc.ballpit.util.MallTeam;
import info.mallmc.ballpit.util.MapManger;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.GameRules;
import info.mallmc.core.api.GameState;
import info.mallmc.core.api.logs.LL;
import info.mallmc.core.api.logs.Log;
import info.mallmc.core.api.logs.Trigger;
import info.mallmc.framework.Framework;
import info.mallmc.framework.api.MallBossBar;
import info.mallmc.framework.events.custom.FrameworkEnableEvent;
import info.mallmc.framework.events.custom.OneMinuteEvent;
import info.mallmc.framework.events.custom.OneSecondEvent;
import info.mallmc.framework.util.MallCommand;
import info.mallmc.framework.util.jukebox.MallJukebox;
import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.logging.Logger;
import net.minecraft.server.v1_12_R1.DedicatedServer;
import net.minecraft.server.v1_12_R1.MinecraftServer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class BallPit extends JavaPlugin implements Listener {

  private static BallPit instance;
  private MallTeam spectatorTeam;
  private Logger logger;
  private MallBossBar mallBossBar;
  private World lobbyWorld;
  private MallJukebox mallJukebox;

  private  IBallPitGame loadedGame;

  public static BallPit getInstance() {
    return instance;
  }

  public MallBossBar getBossBar() {
    return mallBossBar;
  }

  public MallTeam getSpectatorsTeam() {
    return spectatorTeam;
  }

  @Override
  public void onEnable() {
    instance = this;
    logger = Bukkit.getLogger();
    ((DedicatedServer) MinecraftServer.getServer()).propertyManager
        .setProperty("level-name", "lobby_world");
    loadGames();
    getServer().getPluginManager().registerEvents(this, this);
    lobbyWorld = Bukkit.getWorld("lobby_world");
    mallBossBar = new MallBossBar("", BarColor.BLUE, BarStyle.SOLID);

  }

  @EventHandler
  public void onFrameworkEnable(FrameworkEnableEvent event) {
    scanForGames();
    if(loadedGame.getSettings().areSpectatorsEnabled()) {
      spectatorTeam = new MallTeam("Spectator", ChatColor.GOLD);
    }

    MallCommand.registerCommand("ballpit", "gm", new GameModeCommand());
    MallCommand.registerCommand("ballpit", "game", new GameCommand());
    MallCommand.registerCommand("ballpit", "spectator", new Spectator());
    MallCommand.registerCommand("ballpit", "sp", new Spectator());
    MallCommand.registerCommand("ballpit", "map", new MapCommand());
    EventGameStateChange.register();
    CountDownManager.initCountdownHelpers();
    lobbyWorld.setThundering(false);
    lobbyWorld.setStorm(false);
    lobbyWorld.setPVP(false);
    lobbyWorld.setGameRuleValue(GameRules.DO_WEATHER_CYCLE.getName(), "false");
    lobbyWorld.setGameRuleValue(GameRules.DO_MOB_SPAWNING.getName(), "false");
    lobbyWorld.setGameRuleValue(GameRules.DO_DAYLIGHT_CYCLE.getName(), "false");
    lobbyWorld.setDifficulty(Difficulty.PEACEFUL);
    Bukkit.getPluginManager().registerEvents(new PlayerJoinEvents(), this);
    Bukkit.getPluginManager().registerEvents(new CountdownFinishEvent(), this);
    GameState.setGameState(GameState.LOBBY);
    mallJukebox = new MallJukebox("ballpit");
    MallCore.getInstance().getRedis().sendMessage("serverReady", MallCore.getInstance().getServerIP() + ":" + MallCore.getInstance().getServerPort() +":" + MallCore.getInstance().getServer().getNickname().split("-")[0]);
  }

  @EventHandler
  public void onOneSecond(OneSecondEvent event) {
    for(CountdownHelper countdownHelper: CountdownHelper.getCountdownHelpers().values()) {
      if (countdownHelper.isStarted())
        if (!countdownHelper.isPaused()) {
          if (!countdownHelper.isDome()){
            countdownHelper.tickDownCountdown();
          }
        }
    }
    loadedGame.getGameHandler().onOneSecond();
  }

  @EventHandler
  public void onOneMinute(OneMinuteEvent event) {
    loadedGame.getGameHandler().onOneMinute();
  }

  public void handleClass(Class<?> matchingClass)
      throws IllegalAccessException, InstantiationException {
    if (matchingClass.getInterfaces()[0].isAssignableFrom(IBallPitGame.class)) {
      IBallPitGame ballPitGame = (IBallPitGame) matchingClass.newInstance();
      logger.info("Loading " + ballPitGame.getName());
      ballPitGame.onEnable();
      loadedGame = ballPitGame;
    } else {
      Log.error(LL.ERROR, Framework.getInstance().getCurrentServer().getNickname(),
          Trigger.LOAD.name(), "Error while loading ballpit game", "ERROR");
    }
  }

  @Override
  public void onDisable() {
    loadedGame.onDisable();
  }

  private void loadGames() {
    final File folder = new File("games");
    if (!folder.exists()) {
      folder.mkdirs();
    }
    if(folder.listFiles().length == 0) {
        Log.error(LL.INFO, Framework.getInstance().getCurrentServer().getNickname(), "Loading Games", "No Games", "BallPit Loaded With No Game #Why");
    }
    File fileEntry = folder.listFiles()[0];
      try {
        File file = fileEntry;
        URL url = file.toURI().toURL();
        URLClassLoader classLoader = (URLClassLoader) getClass().getClassLoader();
        Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
        method.setAccessible(true);
        method.invoke(classLoader, url);
      } catch (Exception ex) {
        ex.printStackTrace();
      }

  }

  private void scanForGames() {
    new FastClasspathScanner("info.mallmc").matchClassesWithAnnotation(BallPitGame.class, c -> {
      try {
        handleClass(c);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InstantiationException e) {
        e.printStackTrace();
      }
    }).scan();
    PluginManager pl = Bukkit.getPluginManager();
    if (loadedGame.getEvents().size() > 0) {
      int eventCount = 0;
      for (Listener listener : loadedGame.getEvents()) {
        eventCount++;
        pl.registerEvents(listener, this);
      }
      if (eventCount > 0) {
        logger.info(loadedGame.getName() + " registered " + eventCount + " events");
      }

    }
    for (Map map : loadedGame.getMapHelper().getMaps()) {
      MapManger.registerMap(map);
    }
    loadedGame.getMapHelper().setMap();
    GameModeManager.registerGameMode(loadedGame.getGameModes());
    loadedGame.getLobbyHelper().initLobby();
  }

  public MallTeam getSpectatorTeam() {
    return spectatorTeam;
  }

  public IBallPitGame getLoadedGame() {
    return loadedGame;
  }

  public World getLobbyWorld() {
    return lobbyWorld;
  }

  public MallJukebox getMallJukebox() {
    return mallJukebox;
  }
}
