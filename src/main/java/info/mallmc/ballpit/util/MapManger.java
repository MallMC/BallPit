package info.mallmc.ballpit.util;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.IBallPitGame;
import info.mallmc.ballpit.api.Map;
import info.mallmc.core.api.GameState;
import info.mallmc.framework.util.helpers.MathHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class MapManger {

  private static List<Map> registedMaps = new ArrayList<>();

  private static Map currentMap;

  public static Map getCurrentMap() {
    return currentMap;
  }

  public static void setMap(Map activeMap) {
    currentMap = activeMap;
    if(GameState.getGameState() == GameState.LOBBY) {
      for (Player player : Bukkit.getOnlinePlayers())
        BallPit.getInstance().getLoadedGame().getScoreboards().lobby(player);
    }
  }

  public static void registerMap(Map... map)
  {
    for(Map map1: map) {
      registedMaps.add(map1);
    }
  }

  public static List<Map> getRegistedMaps() {
    return registedMaps;
  }

  public static Map getRandomMap() {
    if (registedMaps.size() != 0) {
      return registedMaps.get(MathHelper.randomInt(0, registedMaps.size() - 1));
    }
    return null;
  }

  public static List<String> getMapNames() {
    List<String> mapNames = new ArrayList<>();
    for (Map map : registedMaps) {
      mapNames.add(map.getMapName());
    }
    return mapNames;
  }
}
