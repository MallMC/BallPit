package info.mallmc.ballpit.util;

public enum EnumDisplaySpot
{
  BOSSBAR,
  TITLE,
  SUBTITLE,
  ACTIONBAR,
  CHAT
}
