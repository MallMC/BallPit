package info.mallmc.ballpit.util;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.ISettings;
import info.mallmc.ballpit.util.CountdownHelper;

public class CountDownManager {

  public static CountdownHelper lobbyCountDownHelper;
  public static CountdownHelper preGameCountdownHelper;
  public static CountdownHelper inGameCountdownHelper;

  public static void initCountdownHelpers()
  {
    ISettings activeGameModeSettigs = BallPit.getInstance().getLoadedGame().getSettings();
    lobbyCountDownHelper = new CountdownHelper("lobbyCountdown", activeGameModeSettigs.getLobbyCountdownDisplays(), activeGameModeSettigs.lobbyCountdownTime());
    preGameCountdownHelper = new CountdownHelper("preGameCountdownHelper", activeGameModeSettigs.getPreGameCountdownDisplays(), activeGameModeSettigs.getPreGameCountdownTime());
    inGameCountdownHelper = new CountdownHelper("inGameCountdown", activeGameModeSettigs.getInGameCountdownDisplays(), activeGameModeSettigs.getGameLength());
  }
}
