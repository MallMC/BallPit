package info.mallmc.ballpit.util;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.events.CountdownFinishEvent;
import info.mallmc.ballpit.events.custom.BallPitCountdownFinishEvent;
import info.mallmc.ballpit.events.custom.BallPitSpectatorChangeEvent;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.helpers.MathHelper;
import java.util.HashMap;
import java.util.List;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle.EnumTitleAction;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class CountdownHelper {

  private static final HashMap<String, CountdownHelper> countdownHelpers = new HashMap<>();

  private final String countdownName;
  private List<EnumDisplaySpot> enumDisplaySpots;
  private final int countDownTime;
  private boolean isStarted;
  private boolean isPaused;
  private int countTime;

  public CountdownHelper(String countdownName, List<EnumDisplaySpot> enumDisplaySpots,
      int countDownTime) {
    this.countdownName = countdownName;
    this.enumDisplaySpots = enumDisplaySpots;
    this.countDownTime = countDownTime;
    this.isStarted = false;
    this.isPaused = false;
    this.countTime = countDownTime;
    countdownHelpers.put(countdownName, this);
  }

   public int tickDownCountdown()
  {
    int newAmount = --countTime;
    for(EnumDisplaySpot enumDisplaySpot: enumDisplaySpots)
    {
      display(enumDisplaySpot, newAmount);
    }
    if(isDome())
    {
      BallPitCountdownFinishEvent changeEvent = new BallPitCountdownFinishEvent(this);
      Bukkit.getServer().getPluginManager().callEvent(changeEvent);
    }
    return newAmount;
  }

  public void stop()
  {
    this.isStarted = false;
  }

  public void start()
  {
    this.countTime = countDownTime;
    this.isPaused = false;
    this.isStarted = true;
  }

  public void pause()
  {
    this.isPaused = true;
  }

  public void unPause()
  {
    this.isPaused = false;
  }

  public boolean isPaused() {
    return isPaused;
  }

  public boolean isStarted() {
    return isStarted;
  }
  public boolean isDome()
  {
    return !(countTime > 0);
  }

  public void addDisplaySpot(EnumDisplaySpot displaySpot)
  {
    if(!enumDisplaySpots.contains(displaySpot))
    {
      enumDisplaySpots.add(displaySpot);
    }
  }
  public void remmoveDisplaySpot(EnumDisplaySpot displaySpot)
  {
    if(enumDisplaySpots.contains(displaySpot))
    {
      enumDisplaySpots.remove(displaySpot);
    }
  }
  public void setEnumDisplaySpots(List<EnumDisplaySpot> enumDisplaySpots)
  {
    this.enumDisplaySpots = enumDisplaySpots;
  }


  public static void display(EnumDisplaySpot displaySpot, int time)
  {
    switch (displaySpot) {

      case BOSSBAR:
        BallPit.getInstance().getBossBar().setVisible(true);
        BallPit.getInstance().getBossBar().setTitle(MathHelper.conventIntToTime(time));
        break;
      case TITLE:
        if (time <= 10) {
          for (Player p : Bukkit.getOnlinePlayers()) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            Messaging.sendTitleOrSubtitle(MallPlayer.getPlayer(p.getUniqueId()), EnumTitleAction.TITLE, 0, 0, 20, "ballpit.timer.time", time + "", ChatColor.GOLD + "");
          }
        }
        break;
      case SUBTITLE:
        if (time <= 10) {
          for (Player p : Bukkit.getOnlinePlayers()) {
            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
            Messaging.sendTitleOrSubtitle(MallPlayer.getPlayer(p.getUniqueId()), EnumTitleAction.SUBTITLE, 0, 0, 20, "ballpit.timer.time", time + "", ChatColor.GOLD + "");
          }
        }
        break;
      case ACTIONBAR:
        for (Player p: Bukkit.getOnlinePlayers())
        {
          if(time <= 10)
            Messaging.sendPlayerActionbar(MallPlayer.getPlayer(p.getUniqueId()), "ballpit.timer.time", time+ "");
        }
        break;
      case CHAT:
        if (MathHelper.isDivsableByX(time, 60)) {
          Messaging
              .broadcastMessage("ballpit.timer.minutesLeft", time / 60);
        }
        if (time < 10 || time == 30) {
          Messaging.broadcastMessage("ballpit.timer.secondsLeft", time);
        }
        break;
    }
  }

  public String getCountdownName() {
    return countdownName;
  }

  public static HashMap<String, CountdownHelper> getCountdownHelpers() {
    return countdownHelpers;
  }
}
