package info.mallmc.ballpit.util;

import info.mallmc.ballpit.BallPit;
import info.mallmc.framework.util.Scoreboard;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;

public class MallTeam {

  private static List<MallTeam> registedTeams = new ArrayList<>();
  private List<UUID> players = new ArrayList<>();
  private String teamName;
  private ChatColor color;
  private Team minecraftTeam;
  private int points;

  public MallTeam(String name, ChatColor teamColor) {
    teamName = name;
    color = teamColor;
    this.minecraftTeam = Scoreboard.getScoreboard().registerNewTeam(name);
    this.minecraftTeam.setDisplayName(this.getColor() + name);
    this.minecraftTeam.setPrefix(this.getColor() + "[" + name + "]");
    this.registedTeams.add(this);

  }
  public void setOption(Option option, OptionStatus optionStatus) {
    minecraftTeam.setOption(option, optionStatus);
  }

  public OptionStatus getOption(Option option) {
    return minecraftTeam.getOption(option);
  }

  public int getSize()
  {
    return players.size();
  }

  public String getName() {
    return teamName;
  }

  public int getPoints() {
    return points;
  }

  public void setPoints(int i) {
    points = i;
  }

  public void addPoint() {
    points++;
  }

  public void removePoint() {
    points--;
  }

  public void addPlayer(UUID uuid) {
    this.players.add(uuid);
    this.minecraftTeam.addPlayer(Bukkit.getPlayer(uuid));
  }

  public void removePlayer(UUID uuid) {
   this.players.remove(uuid);
   this.minecraftTeam.removePlayer(Bukkit.getPlayer(uuid));
  }

  public void teleportTeam(Location l) {
    for (UUID uuid: players) {
      Player p = Bukkit.getPlayer(uuid);
      p.teleport(l);
    }
  }

  public ChatColor getColor() {
    return color;
  }
  public boolean hasPlayer(UUID uuid)
  {
    return players.contains(uuid);
  }

  public List<Player> getPlayers() {
    List<Player> players = new ArrayList<>();
    for(UUID uuid: this.players) {
      players.add(Bukkit.getPlayer(uuid));
    }
    return players;
  }

  public String getTeamName() {
    return teamName;
  }

  public static MallTeam getSmallestTeam() {
    MallTeam smallest = registedTeams.get(0);
    for (MallTeam team: registedTeams) {
        if(team.getName().equalsIgnoreCase(BallPit.getInstance().getSpectatorsTeam().getName())){
          continue;
        }
        if (team.getSize() < smallest.getSize()) {
          smallest = team;
      }
    }
    return smallest;
  }

  public static List<MallTeam> getRegistedTeams() {
    return registedTeams;
  }
}
