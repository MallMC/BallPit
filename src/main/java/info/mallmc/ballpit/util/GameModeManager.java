package info.mallmc.ballpit.util;

import info.mallmc.ballpit.api.IGameMode;
import info.mallmc.ballpit.events.custom.BitPitGamemodeChangeEvent;
import info.mallmc.framework.util.helpers.MathHelper;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;

public class GameModeManager
{
    public static List<IGameMode> gamemodes = new ArrayList();

    private static IGameMode activeGamemode;

    public static void registerGameMode(List<IGameMode> gameModes)
    {
      for (IGameMode iGameMode: gameModes) {
        gamemodes.add(iGameMode);
      }
      setActiveGamemode(gameModes.get(MathHelper.randomInt(0, gameModes.size() -1)));
    }

  public static List<IGameMode> getGamemodes() {
    return gamemodes;
  }
  public static List<String> getGamemodeNames()
  {
    List<String> gameModeNames = new ArrayList<>();
    for(IGameMode gm: gamemodes) {
      gameModeNames.add(gm.getName());
    }
    return gameModeNames;
  }

  public static IGameMode getActiveGamemode() {
      if(activeGamemode == null) {
        for (IGameMode gm : gamemodes) {
          activeGamemode = gm;
        }
      }
    return activeGamemode;
  }

  public static boolean setActiveGamemode(IGameMode gameMode) {
      if(gameMode == activeGamemode) {
        return false;
      }
    BitPitGamemodeChangeEvent changeEvent = new BitPitGamemodeChangeEvent(activeGamemode, gameMode);
    Bukkit.getServer().getPluginManager().callEvent(changeEvent);
    GameModeManager.activeGamemode = gameMode;
    return true;
  }
}
