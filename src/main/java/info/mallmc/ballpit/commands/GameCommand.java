package info.mallmc.ballpit.commands;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.util.CountDownManager;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.MallCommand.SenderType;
import info.mallmc.framework.util.Messaging;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.util.StringUtil;

public class GameCommand implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    if (args.length == 0) {
      Messaging.sendMessage(sender, "global.command.incorrectusage", "/game start/stop/restart");
      return false;
    }
    String subcommand = args[0];

    switch (subcommand) {
      case "start":
        BallPit.getInstance().getLoadedGame().getGameManger().setup();
        break;
      case "force":
        if(CountDownManager.lobbyCountDownHelper.isStarted()) {
          CountDownManager.lobbyCountDownHelper.pause();
        }
        BallPit.getInstance().getLoadedGame().getGameManger().startPreGame();
        break;
      case "stop":
        BallPit.getInstance().getLoadedGame().getGameManger().stopGame();
        break;
      default:
        Messaging.sendMessage(sender, "global.command.incorrectusage", "/game start/stop/restart");
        break;
    }

    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] strings) {
    return StringUtil
        .copyPartialMatches(strings[0], Arrays.asList("start", "stop", "force"), new ArrayList<>());
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.STAFF;
  }

  @Override
  public SenderType getSenderType() {
    return SenderType.BOTH;
  }
}
