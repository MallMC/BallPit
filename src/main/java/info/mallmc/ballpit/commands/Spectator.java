package info.mallmc.ballpit.commands;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.core.api.GameState;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.Messaging;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spectator implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender sender, Command command, String s,
      String[] args) {
    if(sender instanceof Player) {
      Player pl = (Player) sender;
      if (GameState.getGameState() == GameState.SETUP|| GameState.getGameState() == GameState.LOBBY) {
        BallPitPlayer b = BallPitPlayer.getBallPitPlayer(((Player) sender).getUniqueId());
        if (b.isSpectator()) {
          int i = Bukkit.getOnlinePlayers().size();
          for (BallPitPlayer bp : BallPitPlayer.getPlayers().values()) {
            if (bp.isSpectator()) {
              i--;
            }
          }
          if (i < BallPit.getInstance().getLoadedGame().getSettings().getMaxPlayers()) {
            b.setSpectator(false);
            Messaging.sendMessage(sender, "ballpit.spectator.leave");
          } else {
            Messaging.sendMessage(sender, "ballpit.spectator.gameFull");
          }
        } else {
          b.setSpectator(true);
          Messaging.sendMessage(sender, "ballpit.spectator.join");
        }
      }else
      {
        Messaging.sendMessage(sender, "ballpit.spectator.gameStarted");
      }
    }
    return true;
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.JNR_STAFF;
  }
}
