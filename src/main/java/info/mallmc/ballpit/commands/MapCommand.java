package info.mallmc.ballpit.commands;

import info.mallmc.ballpit.api.Map;
import info.mallmc.ballpit.util.MapManger;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.core.api.player.PermissionSet;
import info.mallmc.framework.api.IMallExecutor;
import info.mallmc.framework.util.Messaging;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

public class MapCommand implements IMallExecutor {

  @Override
  public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
    if (sender instanceof Player && MallPlayer.getPlayer(((Player) sender).getUniqueId()).getPermissionSet().getPower() < PermissionSet.SNR_STAFF.getPower()) {
      Messaging.sendMessage(sender, "global.command.notallowed");
      return false;
    }
    if (args.length == 0) {
      Messaging.sendMessage(sender, "global.command.incorrectusage", "/map mapName");
      return false;
    }
    String subcommand = args[0];
    for(Map map: MapManger.getRegistedMaps()) {
      if (subcommand.equalsIgnoreCase(map.getMapName())) {
        MapManger.setMap(map);
        return true;
      }
    }
    return true;
  }

  @Override
  public List<String> onTabComplete(CommandSender commandSender, Command command, String s,
      String[] strings) {
    return StringUtil.copyPartialMatches(strings[0], MapManger.getMapNames(), new ArrayList<>());
  }

  @Override
  public PermissionSet getNeedPermissionSet() {
    return PermissionSet.JNR_STAFF;
  }
}
