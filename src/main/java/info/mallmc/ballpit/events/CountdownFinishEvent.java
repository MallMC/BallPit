package info.mallmc.ballpit.events;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.events.custom.BallPitCountdownFinishEvent;
import info.mallmc.ballpit.events.custom.BallPitMapChangeEvent;
import info.mallmc.ballpit.util.CountDownManager;
import info.mallmc.core.api.GameState;
import info.mallmc.core.util.BanUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class CountdownFinishEvent implements Listener
{

  /**
   * Calls other GamePhaes to start after a countDount has finished
   * @param event
   */
  @EventHandler
  public void onCountdownFisnish(BallPitCountdownFinishEvent event) {
    if (event.getFinishedCountdown().getCountdownName()
        .equalsIgnoreCase(CountDownManager.preGameCountdownHelper.getCountdownName())) {
      BallPit.getInstance().getLoadedGame().getGameManger().startGame();
    }
    if (event.getFinishedCountdown().getCountdownName()
        .equalsIgnoreCase(CountDownManager.lobbyCountDownHelper.getCountdownName())) {
      BallPit.getInstance().getLoadedGame().getGameManger().startPreGame();
    }
    if (event.getFinishedCountdown().getCountdownName()
        .equalsIgnoreCase(CountDownManager.inGameCountdownHelper.getCountdownName())) {
      BallPit.getInstance().getLoadedGame().getGameManger().stopGame();
    }
  }


  /**
   * Updates the lobby scoreboard if the Map changes
   * @param event
   */
  @EventHandler
  public void mapChangeEvent(BallPitMapChangeEvent event)
  {
    if(GameState.getGameState() == GameState.LOBBY) {
      for(Player player: Bukkit.getOnlinePlayers()) {
        BallPit.getInstance().getLoadedGame().getScoreboards().lobby(player);

      }
    }
  }
}
