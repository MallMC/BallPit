package info.mallmc.ballpit.events;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.IBallPitGame;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.managers.BallPitScoreboards;
import info.mallmc.ballpit.managers.LobbyHelper;
import info.mallmc.core.api.GameState;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.helpers.ServerSending;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerJoinEvents implements Listener {

  @EventHandler
  public void onPlayerLogin(PlayerLoginEvent event) {
    BallPitPlayer.getBallPitPlayer(event.getPlayer().getUniqueId());
    switch (GameState.getGameState())
    {
      case ENDING:
        ServerSending.sendToServer(event.getPlayer(), "hub1");
        break;
    }

  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent event) {
    MallPlayer mp = MallPlayer.getPlayer(event.getPlayer().getUniqueId());
    BallPitPlayer bp = BallPitPlayer.getBallPitPlayer(event.getPlayer().getUniqueId());
    Player player = event.getPlayer();
    int maxPlayer = BallPit.getInstance().getLoadedGame().getSettings().getMaxPlayers();
    if (BallPit.getInstance().getLoadedGame().getSettings().hasResourcePack()) {
      event.getPlayer().setResourcePack(BallPit.getInstance().getLoadedGame().getSettings().resourcePackURL());
    }
    BallPit.getInstance().getMallJukebox().addPlayer(event.getPlayer());
    BallPit.getInstance().getBossBar().addPlayer(mp);
    switch (GameState.getGameState()) {

      case LOBBY:
      case SETUP:
        player.getInventory().clear();
        player.setGameMode(GameMode.ADVENTURE);
        player.getActivePotionEffects().clear();
        player.setLevel(0);
        //Gives player LobbyItems
        for (int i = 0; i < BallPit.getInstance().getLoadedGame().getItemManger().getLobbyItems(player).length; i++) {
          if (BallPit.getInstance().getLoadedGame().getItemManger().getLobbyItems(player)[i]
              == null) {
            continue;
          }
          player.getInventory()
              .setItem(i,
                  BallPit.getInstance().getLoadedGame().getItemManger().getLobbyItems(player)[i]);
        }

        event.getPlayer().teleport(BallPit.getInstance().getLoadedGame().getLobbyHelper().getLobbySpawnLocation());

        //Makes player spectator if players count is over MaxPlayers
        int playerCount = BallPitPlayer.getPlayers().size();
        for(BallPitPlayer ballPitPlayer: BallPitPlayer.getPlayers().values()) {
          if (!ballPitPlayer.isSpectator()) {
            playerCount--;
          }
        }
        if (playerCount > maxPlayer) {
          bp.setSpectator(true);
        }

        //Updates scoreboard for player
        for(Player onlinePlayer: Bukkit.getOnlinePlayers()) {
          BallPit.getInstance().getLoadedGame().getScoreboards().lobby(onlinePlayer);
        }
        break;

      case IN_GAME:

        //Sets player spectator
        if (!bp.hasTeam()) {
          bp.setSpectator(true);
        }
        for(Player onlinePlayer: Bukkit.getOnlinePlayers()) {
          BallPit.getInstance().getLoadedGame().getScoreboards().game(onlinePlayer);
        }
        break;
    }

  }
  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent event) {
    BallPitPlayer leaveingBallPlayer = BallPitPlayer.getBallPitPlayer(event.getPlayer().getUniqueId());
    leaveingBallPlayer.remove();
    BallPit.getInstance().getMallJukebox().removePlayer(event.getPlayer());
    BallPit.getInstance().getBossBar().removePlayer(MallPlayer.getPlayer(event.getPlayer().getUniqueId()));
    switch (GameState.getGameState()) {
      case LOBBY:
        for (BallPitPlayer ballPitPlayer : BallPitPlayer.getPlayers().values()) {
          BallPit.getInstance().getLoadedGame().getScoreboards().lobby(ballPitPlayer.getPlayer());
        }
        break;
      case IN_GAME:
        for(Player onlinePlayer: Bukkit.getOnlinePlayers()) {
          BallPit.getInstance().getLoadedGame().getScoreboards().game(onlinePlayer);
        }
        break;
    }
  }
}
