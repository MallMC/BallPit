package info.mallmc.ballpit.events.core;

import info.mallmc.ballpit.BallPit;
import info.mallmc.core.MallCore;
import info.mallmc.core.api.GameState;
import info.mallmc.core.events.Event;
import info.mallmc.core.events.EventHandler;
import info.mallmc.core.events.EventType;
import info.mallmc.framework.util.helpers.ServerSending;
import org.bukkit.Bukkit;

public class EventGameStateChange
{
  public static void register() {
    EventHandler.registerEvent(EventType.GAMESTATE_CHANGE_EVENT, event -> updateScoreboard());
  }

  /**
   * Used to make sure this code is always ran
   * And do to that fact that more then 1 spots may change the GamePhase removes the need to code to be used more then once
   */
  private static void updateScoreboard() {
    switch (GameState.getGameState())
    {
      case SETUP:
        MallCore.getInstance().getRedis().sendMessage("gameStart", MallCore.getInstance().getServerIP() + ":" + MallCore.getInstance().getServerPort() + ":" + MallCore.getInstance().getServer().getNickname().split("-")[0]);
        break;
      case ENDING:
        Bukkit.getScheduler().runTaskLater(
            BallPit.getInstance(), () -> ServerSending.sendToServer("hub1"), 200);
        MallCore.getInstance().getRedis().sendMessage("gameFinished", MallCore.getInstance().getServerIP() + ":" + MallCore.getInstance().getServerPort());
        Bukkit.getScheduler().runTaskLater(
            BallPit.getInstance(), () ->  Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop"), 400);
    }
  }
}
