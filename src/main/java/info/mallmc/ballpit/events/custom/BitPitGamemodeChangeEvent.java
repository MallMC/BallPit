package info.mallmc.ballpit.events.custom;

import info.mallmc.ballpit.api.IGameMode;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BitPitGamemodeChangeEvent extends Event
{

  public static final HandlerList handlers = new HandlerList();

  private IGameMode newGameMode;
  private IGameMode oldGameMode;

  public BitPitGamemodeChangeEvent(IGameMode newGameMode, IGameMode oldGameMode) {
    this.newGameMode = newGameMode;
    this.oldGameMode = oldGameMode;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public IGameMode getNewGameMode() {
    return newGameMode;
  }

  public IGameMode getOldGameMode() {
    return oldGameMode;
  }
}
