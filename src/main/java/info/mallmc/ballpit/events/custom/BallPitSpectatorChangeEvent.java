package info.mallmc.ballpit.events.custom;

import info.mallmc.core.api.player.MallPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BallPitSpectatorChangeEvent extends Event {


  public static final HandlerList handlers = new HandlerList();

  private MallPlayer player;
  private boolean isNowSpectator;

  public BallPitSpectatorChangeEvent(MallPlayer player, boolean isNowSpectator) {
    this.player = player;
    this.isNowSpectator = isNowSpectator;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public MallPlayer getPlayer() {
    return player;
  }

  public boolean isNowSpectator() {
    return isNowSpectator;
  }
}
