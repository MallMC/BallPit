package info.mallmc.ballpit.events.custom;

import info.mallmc.ballpit.api.Map;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BallPitMapChangeEvent extends Event {

  public static final HandlerList handlers = new HandlerList();

  private Map oldMap;
  private Map newMap;


  public BallPitMapChangeEvent(Map oldMap, Map newMap) {
    this.oldMap = oldMap;
    this.newMap = newMap;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public Map getNewMap() {
    return newMap;
  }

  public Map getOldMap() {
    return oldMap;
  }
}
