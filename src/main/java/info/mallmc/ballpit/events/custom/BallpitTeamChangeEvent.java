package info.mallmc.ballpit.events.custom;

import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.util.MallTeam;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BallpitTeamChangeEvent extends Event{

  public static final HandlerList handlers = new HandlerList();

  private BallPitPlayer ballPitPlayer;
  private MallTeam mallTeam;


  public BallpitTeamChangeEvent(BallPitPlayer ballPitPlayer, MallTeam mallTeam) {
    this.ballPitPlayer = ballPitPlayer;
    this.mallTeam = mallTeam;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public BallPitPlayer getBallPitPlayer() {
    return ballPitPlayer;
  }

  public MallTeam getNewTeam() {
    return mallTeam;
  }
}
