package info.mallmc.ballpit.events.custom;

import info.mallmc.ballpit.api.Map;
import info.mallmc.ballpit.util.CountdownHelper;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BallPitCountdownFinishEvent extends Event {

  public static final HandlerList handlers = new HandlerList();

  private CountdownHelper countdownHelper;

  public BallPitCountdownFinishEvent(CountdownHelper countdownHelper) {
    this.countdownHelper = countdownHelper;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }

  public CountdownHelper getFinishedCountdown() {
    return countdownHelper;
  }
}
