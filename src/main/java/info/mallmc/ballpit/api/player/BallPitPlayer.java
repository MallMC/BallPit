package info.mallmc.ballpit.api.player;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.IPowerup;
import info.mallmc.ballpit.events.custom.BallPitSpectatorChangeEvent;
import info.mallmc.ballpit.events.custom.BallpitTeamChangeEvent;
import info.mallmc.ballpit.util.MallTeam;
import info.mallmc.core.api.player.MallPlayer;
import java.util.HashMap;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;

public class BallPitPlayer
{
  private static HashMap<UUID, BallPitPlayer> players = new HashMap<>();

  private boolean isSpectator;
  @Nullable
  private IPowerup powerup;
  private UUID uuid;
  @Nullable
  private MallTeam mallTeam;

  public static BallPitPlayer getBallPitPlayer(UUID uuid) {
    if (!players.containsKey(uuid)) {
      BallPitPlayer bp = new BallPitPlayer(uuid);
      players.put(uuid, bp);
      return bp;
      }
    return players.get(uuid);
  }

  public void remove() {
    if (mallTeam != null) {
      mallTeam.removePlayer(uuid);
    }
    players.remove(this);
  }

  BallPitPlayer(UUID uuid)
  {
    this.isSpectator = false;
    this.powerup = null;
    this.uuid = uuid;
  }

  public boolean isSpectator() {
    return isSpectator;
  }

  public void setSpectator(boolean spectator) {
    MallPlayer mp = MallPlayer.getPlayer(getPlayer().getUniqueId());
    BallPitSpectatorChangeEvent changeEvent;
    mp.setInSpecChat(spectator);
    if (spectator) {
      getPlayer().setGameMode(GameMode.SPECTATOR);
      setTeam(BallPit.getInstance().getSpectatorTeam());
      mp.setChatPrefix(BallPit.getInstance().getSpectatorTeam().getName());
      changeEvent = new BallPitSpectatorChangeEvent(MallPlayer.getPlayer(getPlayer().getUniqueId()), true);
    } else {
      changeEvent = new BallPitSpectatorChangeEvent(MallPlayer.getPlayer(getPlayer().getUniqueId()), false);
      getPlayer().setGameMode(GameMode.ADVENTURE);
      clearTeam();
    }
    this.isSpectator = spectator;
    Bukkit.getServer().getPluginManager().callEvent(changeEvent);
  }

  public static HashMap<UUID, BallPitPlayer> getPlayers() {
    return players;
  }

  @Nullable
  public IPowerup getPowerup() {
    return powerup;
  }

  public void setPowerup(@Nullable IPowerup powerup) {
    this.powerup = powerup;
  }

  public UUID getUUID() {
    return uuid;
  }
  public Player getPlayer() {
    return Bukkit.getPlayer(uuid);
  }

  public void setTeam(MallTeam mallTeam) {
    clearTeam();
    this.mallTeam = mallTeam;
    this.mallTeam.addPlayer(uuid);
    BallpitTeamChangeEvent teamChangeEvent = new BallpitTeamChangeEvent(this, mallTeam);
    Bukkit.getServer().getPluginManager().callEvent(teamChangeEvent);
  }

  public MallTeam getTeam() {
    return mallTeam;
  }

  public boolean hasTeam() {
    return mallTeam != null;
  }
  public void clearTeam()
  {
    if(hasTeam()) {
      if(this.mallTeam.hasPlayer(uuid)) {
        this.mallTeam.removePlayer(uuid);
        this.mallTeam = null;
      }
    }
  }




}
