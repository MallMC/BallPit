package info.mallmc.ballpit.api;

public class GamePhase
{
  private final String name;
  public static GamePhase gamePhase;

  public GamePhase(String name)
  {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public static GamePhase getGamePhase() {
    return gamePhase;
  }

  public static void setGamePhase(GamePhase gamePhase) {
    GamePhase.gamePhase = gamePhase;
  }
}
