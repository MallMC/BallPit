package info.mallmc.ballpit.api;

import java.util.List;
import org.bukkit.entity.Player;

public interface IPowerup
{
    String getName();
    int getUseLength();
    void onPickup(Player player);
    void onRunout(Player player);
    List<GamePhase> getGamePhases();
}
