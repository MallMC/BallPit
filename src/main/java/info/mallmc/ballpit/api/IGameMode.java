package info.mallmc.ballpit.api;


import info.mallmc.core.api.GameState;

public interface IGameMode
{

  String getName();
  void oneSecond(GameState gameState);
  void oneMinute(GameState gameState);
  void onRegister();
}
