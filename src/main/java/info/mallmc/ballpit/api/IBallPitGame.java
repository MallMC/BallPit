package info.mallmc.ballpit.api;

import info.mallmc.ballpit.managers.BallPitScoreboards;
import info.mallmc.ballpit.managers.GameHandler;
import info.mallmc.ballpit.managers.GameStageManger;
import info.mallmc.ballpit.managers.ItemManager;
import info.mallmc.ballpit.managers.LobbyHelper;
import info.mallmc.ballpit.managers.MapHelper;
import java.util.List;
import org.bukkit.event.Listener;

public interface IBallPitGame {


    String getName();

    default String getDisplayName() {
        return getName();
    }

    void onEnable();

    void onDisable();

    List<IGameMode> getGameModes();

    List<Listener> getEvents();

    GameHandler getGameHandler();

    ISettings getSettings();

    GameStageManger getGameManger();

    MapHelper getMapHelper();

    LobbyHelper getLobbyHelper();

    BallPitScoreboards getScoreboards();

    ItemManager getItemManger();
}
