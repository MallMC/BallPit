package info.mallmc.ballpit.api;

import info.mallmc.ballpit.util.MallTeam;
import java.util.HashMap;
import org.bukkit.Location;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;

import java.util.List;

public class Map {

  // Now this....
  // Maps can have info based on GamePhase
  //If you do not get this i need to explan this over voice so just tell me
  // I really don't know if all the GamePhase stuff is need but it's there just i case
  //if you have ideas how to make it better let me know to



  private String mapName;
  private List<String> authors;
  private HashMap<GamePhase, Location> mapCenterLocations;
  @Nullable
  private HashMap<GamePhase, List<Location>> powerUpLocations;
  @Nullable
  private HashMap<GamePhase, HashMap<MallTeam, List<Location>>> teamSpawnLocations;

  public Map(String mapName, List<String> authors, HashMap<GamePhase, Location> mapCenterLocations) {
    this.mapName = mapName;
    this.authors = authors;
    this.mapCenterLocations  = mapCenterLocations;
  }
  public Map(String mapName, List<String> authors, HashMap<GamePhase, Location> mapCenterLocations, HashMap<GamePhase, List<Location>> powerUpLocations) {
    this.mapName = mapName;
    this.authors = authors;
    this.mapCenterLocations = mapCenterLocations;
    this.powerUpLocations = powerUpLocations;
  }
  public Map(String mapName, List<String> authors, HashMap<GamePhase, Location> mapCenterLocations, HashMap<GamePhase, HashMap<MallTeam, List<Location>>> teamSpawnLocations, boolean nothing)
  {
    this.mapName = mapName;
    this.authors = authors;
    this.mapCenterLocations = mapCenterLocations;
    this.teamSpawnLocations = teamSpawnLocations;
  }
  public Map(String mapName, List<String> authors, HashMap<GamePhase, Location> mapCenterLocations, HashMap<GamePhase, HashMap<MallTeam, List<Location>>> teamSpawnLocations, HashMap<GamePhase, List<Location>> powerUpLocations)
  {
    this.mapName = mapName;
    this.authors = authors;
    this.mapCenterLocations = mapCenterLocations;
    this.teamSpawnLocations = teamSpawnLocations;
    this.powerUpLocations = powerUpLocations;
  }


  public String getMapName() {
    return mapName;
  }

  public List<String> getAuthors() {
    return authors;
  }

  @Nullable
  public HashMap<GamePhase, Location> getMapCenterLocations() {
    return mapCenterLocations;
  }

  @Nullable
  public HashMap<GamePhase, List<Location>> getPowerUpLocations() {
    return powerUpLocations;
  }

  @Nullable
  public HashMap<GamePhase, HashMap<MallTeam, List<Location>>> getTeamSpawnLocations() {
    return teamSpawnLocations;
  }
}
