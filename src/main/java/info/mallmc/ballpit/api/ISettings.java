package info.mallmc.ballpit.api;

import info.mallmc.ballpit.util.EnumDisplaySpot;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import org.bukkit.Location;

public interface ISettings {

    default boolean areSpectatorsEnabled() { return true; }
    default int getMinPlayers() { return 2; }
    default int getMaxPlayers() { return -1;}
    default int getMaxPlayersPerTeam() {return -1; }
    default int getMinPlayersPerTeam() {return 1; }
    default int lobbyCountdownTime() {return 60;}
    default boolean hasResourcePack() { return false; }

    @Nullable
    default String resourcePackURL() { return null; }

    default List<EnumDisplaySpot> getPreGameCountdownDisplays() {
        return Arrays.asList(EnumDisplaySpot.TITLE, EnumDisplaySpot.CHAT);
    }

    default List<EnumDisplaySpot> getLobbyCountdownDisplays() {
        return Arrays.asList(EnumDisplaySpot.BOSSBAR, EnumDisplaySpot.CHAT);
    }

    default List<EnumDisplaySpot> getInGameCountdownDisplays() {
        return Arrays.asList(EnumDisplaySpot.BOSSBAR, EnumDisplaySpot.CHAT);
    }

    default int getPreGameCountdownTime() {return 10;}

    //In Secs
    int getGameLength();

    default boolean isPowerupsEnabled()
    {
        return true;
    }

    @Nullable
    default List<IPowerup> getPowerups()
    {
        return null;
    }

    default int powerupSpawnRate()
    {
        return 30;
    }

    default boolean hasMusic() {
        return false;
    }

    @Nullable
    default String getMusicURL() {
        return null;
    }


}
