package info.mallmc.ballpit.managers;

import info.mallmc.ballpit.util.GameModeManager;
import info.mallmc.core.api.GameState;

public class GameHandler
{

  /**
   * This i called every minute no matter what
   * It's Calling the onMinute for the GameMode
   * it's called here as you may for some reason want to override in you game
   * if you want it just make sure you have super.onOneMinute();
   */
  public void onOneMinute() {
    GameModeManager.getActiveGamemode().oneMinute(GameState.getGameState());
  }

  /**
   * Same as onOneMinute
   */
  public void onOneSecond() {
    GameModeManager.getActiveGamemode().oneSecond(GameState.getGameState());
  }

}
