package info.mallmc.ballpit.managers;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.player.BallPitPlayer;
import info.mallmc.ballpit.util.GameModeManager;
import info.mallmc.ballpit.util.MapManger;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.SimpleScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class BallPitScoreboards
{


  /**
   * Gets the scoreboard for the lobby
   * @param player
   */
  public void lobby(Player player) {
    if (player == null) {
      return;
    }
    if (SimpleScoreboard.getManager().getScoreboard(player) == null) {
      SimpleScoreboard.getManager().setupBoard(player);
    }
    if (SimpleScoreboard.getManager().getScoreboard(player) != null) {
      SimpleScoreboard.getManager().getScoreboard(player).unregister();
      SimpleScoreboard.getManager().setupBoard(player);
    }
    SimpleScoreboard.getManager().getScoreboard(player).setHeader(Messaging.colorizeMessage("&3Mall&6MC"));
    String mapName;
    if(MapManger.getCurrentMap() != null) {
      mapName = MapManger.getCurrentMap().getMapName();
    }else {
      mapName = "None";
    }
    int size = BallPitPlayer.getPlayers().size();
    for(BallPitPlayer ballPitPlayer: BallPitPlayer.getPlayers().values()) {
      if (ballPitPlayer.isSpectator()) {
        size--;
      }
    }
    SimpleScoreboard.getManager().getScoreboard(player).setScores(
        Messaging.colorizeMessage("&3GameMode: &6" + GameModeManager.getActiveGamemode().getName()),
        Messaging.colorizeMessage("&3Map: " + "&6&l" + mapName),
        Messaging.colorizeMessage("&3Players: " + "&6&l" + size + "/"+ BallPit.getInstance().getLoadedGame().getSettings().getMaxPlayers()));
    SimpleScoreboard.getManager().getScoreboard(player).update();
  }

  /**
   * Gets the scoreboard for ingame
   * @param player
   */
  public void game(Player player) {

  }

}
