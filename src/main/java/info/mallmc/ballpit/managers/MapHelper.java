package info.mallmc.ballpit.managers;

import info.mallmc.ballpit.api.Map;
import info.mallmc.ballpit.util.MapManger;
import java.util.Arrays;
import java.util.List;

public class MapHelper {


  /**
   * Get the maps the way you want to
   * @return
   */
  public List<Map> getMaps() {
    return Arrays.asList();
  }


  /**
   * Called after getMaps pick how you want to set the map to the server
   */
  public void setMap() {
    MapManger.setMap(MapManger.getRandomMap());
  }
}
