package info.mallmc.ballpit.managers;

import info.mallmc.framework.util.items.FrameworkItems;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ItemManager
{

  /**
   * List of items that are given to a player a point
   * @return
   */
    public ItemStack[] getLobbyItems(Player player) {
      ItemStack[] items = new ItemStack[9];
      items[8] = FrameworkItems.backToHub();
      return items;
    }
}
