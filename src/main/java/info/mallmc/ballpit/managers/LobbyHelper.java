package info.mallmc.ballpit.managers;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.api.GamePhase;
import info.mallmc.ballpit.api.Map;
import info.mallmc.framework.util.helpers.LocationHelper;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

public class LobbyHelper
{
  private Location lobbySpawnLocation;


  /**
   * Used to int the lobby and get is Info
   */
  public void initLobby()
  {
    File mainLocation = BallPit.getInstance().getLobbyWorld().getWorldFolder();
    try {
      JsonReader reader = new JsonReader(new FileReader(mainLocation.getPath() + "/lobbyInfo.json"));
      Gson gson = new Gson();
      LobbyInfo laserMap = gson.fromJson(reader, LobbyInfo.class);
      BallPit.getInstance().getLobbyWorld().setTime(laserMap.getWorldTime());
      lobbySpawnLocation = LocationHelper.getDeserializedLocation(BallPit.getInstance().getLobbyWorld(), laserMap.getSpawnLocations());
      reader.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Location getLobbySpawnLocation() {
    return lobbySpawnLocation;
  }

  protected class LobbyInfo
  {
    private String lobbyName;
    private List<String> authors;
    private String spawnLocation;
    private int worldTime;
    private boolean changeTime;

    public List<String> getAuthors() {
      return authors;
    }

    public String getLobbyName() {
      return lobbyName;
    }

    public String getSpawnLocations() {
      return spawnLocation;
    }

    public int getWorldTime() {
      return worldTime;
    }

    public boolean isChangeTime() {
      return changeTime;
    }
  }


}
