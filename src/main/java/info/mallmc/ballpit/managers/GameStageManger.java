package info.mallmc.ballpit.managers;

import info.mallmc.ballpit.BallPit;
import info.mallmc.ballpit.util.CountDownManager;
import info.mallmc.core.api.GameState;
import info.mallmc.core.api.player.MallPlayer;
import info.mallmc.framework.util.Messaging;
import info.mallmc.framework.util.jukebox.MallMedia;
import net.minecraft.server.v1_12_R1.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class GameStageManger {


  /**
   * Call when you want to start lobby countdown
   * Not called if game is forced
   */
  public void setup()
  {
    GameState.setGameState(GameState.SETUP);
    CountDownManager.lobbyCountDownHelper.start();
  }

  /**
   * Call when you start the game
   * always called
   */
  public void startGame()
  {
    GameState.setGameState(GameState.IN_GAME);
    for (MallPlayer mp : MallPlayer.getPlayers().values()) {
      Messaging.sendTitleOrSubtitle(mp, PacketPlayOutTitle.EnumTitleAction.TITLE, 0, 0, 20, "ballpit.game.general.started");
    }
    Messaging.broadcastMessage("ballpit.game.general.started");
    if(BallPit.getInstance().getLoadedGame().getSettings().hasMusic()) {
        BallPit.getInstance().getMallJukebox().playSoundForShow(new MallMedia(BallPit.getInstance().getLoadedGame().getSettings().getMusicURL()));
    }
    CountDownManager.inGameCountdownHelper.start();
  }

  /**
   * Call when you want to start the phase before the game start but after the lobby starts
   * Always called
   */
  public void startPreGame()
  {
    GameState.setGameState(GameState.SETUP);
    CountDownManager.preGameCountdownHelper.start();
  }


  /**
   * Call when you want to end the game
   * Always called
   */
  public void stopGame()
  {
    GameState.setGameState(GameState.ENDING);
    Messaging.broadcastMessage("ballpit.game.general.ended");
    for (MallPlayer mp : MallPlayer.getPlayers().values()) {
      Player p = Bukkit.getPlayer(mp.getUuid());
      p.getInventory().clear();
      Messaging.sendTitleOrSubtitle(mp, PacketPlayOutTitle.EnumTitleAction.TITLE, 0, 0, 20, "ballpit.game.general.ended");
    }
    Messaging.broadcastRawMessage("ballpit.game.end.restarting");
  }
}
